# News scraper
[![Badge pipeline](https://gitlab.com/svenvNL/news-scraper/badges/master/pipeline.svg)](https://gitlab.com/svenvNL/news-scraper/pipelines)

News Scraper gets posts from Hacker News, Reddit and Blogs, filters these and publishes an RSS feed. This software is written in a few days as a proof of concept and is by no means production ready!

## Running
This app uses an PostgreSQL database.

To run this application, run:
```sh
docker run registry.gitlab.com/svenvnl/news-scraper
```

Make sure the following environment variables are set if the default values are not correct.

| variable          | default value |
|-------------------|---------------|
| DATABASE_HOST     | localhost     |
| DATABASE_PORT     | 5432          |
| DATABASE_NAME     | news-scraper  |
| DATABASE_USERNAME | news-scraper  |
| DATABASE_PASSWORD | news-scraper  |
