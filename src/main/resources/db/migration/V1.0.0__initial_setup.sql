CREATE TABLE posts (
    url             VARCHAR(250)    NOT NULL PRIMARY KEY,
    title           VARCHAR(250)    NOT NULL,
    addition_date   TIMESTAMP       NOT NULL,
    source          VARCHAR(11)     NOT NULL,
    sub_source      VARCHAR(50)     NULL,
    category        VARCHAR(20)     NOT NULL
)
