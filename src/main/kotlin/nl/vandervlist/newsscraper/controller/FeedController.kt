package nl.vandervlist.newsscraper.controller

import nl.vandervlist.newsscraper.config.FeedConfig
import nl.vandervlist.newsscraper.service.FeedService
import nl.vandervlist.newsscraper.service.PostService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/feed")
class FeedController @Autowired constructor(
        val postService: PostService,
        val feedService: FeedService,
        val feedConfig: FeedConfig
) {

    @RequestMapping("", produces = ["application/rss+xml"])
    fun getFeed(): String = feedService.buildFeed(feedConfig.name, postService.getLatestPosts())

    @RequestMapping("/{category}", produces = ["application/rss+xml"])
    fun getFeedByCategory(@PathVariable("category") category: String) =
            feedService.buildFeed("${feedConfig.name} - ${category.capitalize()}", postService.getLatestPostsByCategory(category))
}
