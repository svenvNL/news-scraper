package nl.vandervlist.newsscraper.service

import nl.vandervlist.newsscraper.config.BlockConfig
import nl.vandervlist.newsscraper.config.RedditScraperConfig
import nl.vandervlist.newsscraper.config.SubRedditConfig
import nl.vandervlist.newsscraper.model.Post
import nl.vandervlist.newsscraper.model.RedditResponse
import nl.vandervlist.newsscraper.repository.PostsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import java.time.Duration
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

@Service
class RedditScraperService @Autowired constructor(
        blockConfig: BlockConfig,
        postsRepository: PostsRepository,
        restTempleteBuilder: RestTemplateBuilder,
        val redditScraperConfig: RedditScraperConfig
): Scraper(
        blockConfig,
        postsRepository
) {
    val restTemplate: RestTemplate = restTempleteBuilder.build()

    override fun loadPosts() = redditScraperConfig.subreddits.map { getSubreddit(it) }.flatten()

    fun getSubreddit(config: SubRedditConfig): List<Post> {
        val headers = HttpHeaders()
        headers.add("user-agent", "Mozilla/5.0 Firefox/26.0")
        val result = restTemplate.exchange(
                "https://reddit.com/${config.name}.json?count=50",
                HttpMethod.GET,
                HttpEntity("parameters", headers),
                RedditResponse::class.java
        ).body ?: return emptyList()

        return result.data.children
                .asSequence()
                .filter {
                    Duration.between(Timestamp(it.data.created * 1000)
                            .toLocalDateTime()
                            .atZone(ZoneId.systemDefault())
                            .toInstant(), Instant.now()).toMinutes() <= 60 * config.maxAge &&
                            it.data.ups - it.data.downs >= config.minScore
                }
                .filter { !it.data.is_self && !it.data.is_reddit_media_domain }
                .filter {
                    if (config.flair.exclude.isEmpty()) return@filter true
                    !config.flair.exclude.contains(it.data.author_flair_text)
                }
                .filter {
                    if (config.flair.include.isEmpty()) return@filter true
                    config.flair.include.contains(it.data.author_flair_text)
                }
                .map {
                    Post(
                            it.data.url,
                            it.data.title.take(250),
                            LocalDateTime.now(),
                            "Reddit",
                            it.data.subreddit_name_prefixed,
                            config.category
                    )
                }
                .toList()
    }
}
