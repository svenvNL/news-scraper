package nl.vandervlist.newsscraper.service

import nl.vandervlist.newsscraper.repository.PostsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PostService @Autowired constructor(
        val postsRepository: PostsRepository
) {
    fun getLatestPosts(limit: Int = 50) = postsRepository.getLatest(limit)

    fun getLatestPostsByCategory(category: String, limit: Int = 50) = postsRepository.getLatestByCategory(limit, category)
}
