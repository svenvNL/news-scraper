package nl.vandervlist.newsscraper.service

import com.rometools.rome.feed.synd.SyndCategoryImpl
import com.rometools.rome.feed.synd.SyndEntryImpl
import com.rometools.rome.feed.synd.SyndFeedImpl
import com.rometools.rome.io.SyndFeedOutput
import nl.vandervlist.newsscraper.model.Post
import org.springframework.stereotype.Service
import java.time.ZoneId
import java.util.*

@Service
class FeedService {
    fun buildFeed(name: String, posts: List<Post>): String {
        val feed = SyndFeedImpl().apply {
            feedType = "atom_1.0"
            title = name
            entries = posts.map {
                SyndEntryImpl().apply {
                    title = it.title
                    uri = it.url
                    link = it.url
                    publishedDate = Date.from(it.additionDate.atZone(ZoneId.of("UTC")).toInstant())
                    author = if (it.subSource != null) "${it.source} - ${it.subSource}" else it.source
                    categories = listOf(SyndCategoryImpl().apply { setName(it.category) })
                }
            }
        }

        return SyndFeedOutput().outputString(feed)
    }
}
