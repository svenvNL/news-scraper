package nl.vandervlist.newsscraper.service

import nl.vandervlist.newsscraper.config.BlockConfig
import nl.vandervlist.newsscraper.model.Post
import nl.vandervlist.newsscraper.repository.PostsRepository
import java.net.URL

abstract class Scraper constructor(
        private val blockConfig: BlockConfig,
        private val postsRepository: PostsRepository
) {
    protected abstract fun loadPosts(): List<Post>

    fun getPosts(): Int {
        val posts = loadPosts().filter { post ->
            try {
                val categoryBlockConfig = blockConfig.categories.first { it.category == post.category }
                !categoryBlockConfig.domains.contains(URL(post.url).host)
            } catch(e: NoSuchElementException) {
                true
            }
        }.filter { post ->
            try {
                val categoryBlockConfig = blockConfig.categories.first { it.category == post.category }

                categoryBlockConfig.titles.forEach {
                    if (post.title.toLowerCase().contains(it.toLowerCase())) return@filter false
                }

                true
            } catch(e: NoSuchElementException) {
                true
            }
        }

        return if (posts.isNotEmpty()) {
            postsRepository.addPosts(posts)
        } else {
            0
        }
    }
}
