package nl.vandervlist.newsscraper.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("scraper.feed")
class FeedScraperConfig {
    var feeds = emptyList<FeedItemConfig>()
}

class FeedItemConfig {
    var url = ""
    var category = ""
    var include = emptyList<String>()
    var exclude = emptyList<String>()
}
