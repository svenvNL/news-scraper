package nl.vandervlist.newsscraper.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("scraper.hacker-news")
class HackerNewsScraperConfig {
    var maxAge = 0
    var minScore = 0
    var category = ""
    var include = emptyList<String>()
    var exclude = emptyList<String>()
}
