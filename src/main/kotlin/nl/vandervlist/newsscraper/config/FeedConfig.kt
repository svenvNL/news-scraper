package nl.vandervlist.newsscraper.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("feed")
class FeedConfig {
    var name = ""
}
