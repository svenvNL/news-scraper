package nl.vandervlist.newsscraper.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("block")
class BlockConfig {
    var categories = emptyList<BlockConfigCategory>()
}

class BlockConfigCategory {
    var category = ""
    var titles = emptyList<String>()
    var domains = emptyList<String>()
}
