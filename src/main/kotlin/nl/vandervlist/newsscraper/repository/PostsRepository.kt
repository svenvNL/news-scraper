package nl.vandervlist.newsscraper.repository

import nl.vandervlist.newsscraper.model.Post
import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select
import org.springframework.stereotype.Repository

@Repository
interface PostsRepository {

    @Select(
            "SELECT * FROM posts " +
            "ORDER BY addition_date DESC " +
            "LIMIT #{limit}"
    )
    fun getLatest(limit: Int): List<Post>

    @Select(
            "SELECT * FROM posts " +
            "WHERE category = #{category}" +
            "ORDER BY addition_date DESC " +
            "LIMIT #{limit}"
    )
    fun getLatestByCategory(limit: Int, category: String): List<Post>

    @Insert(
            "<script>" +
            "   INSERT INTO posts (url, title, addition_date, source, sub_source, category) VALUES " +
            "       <foreach item='post' collection='posts' open='' separator=',' close=''>" +
            "       (" +
            "           #{post.url}," +
            "           #{post.title}," +
            "           #{post.additionDate}," +
            "           #{post.source}," +
            "           #{post.subSource}," +
            "           #{post.category}" +
            "       )" +
            "       </foreach>" +
            "   ON CONFLICT(url) DO NOTHING" +
            "</script>"
    )
    fun addPosts(@Param("posts") posts: List<Post>): Int

}
